package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@Data
@TableName("prize_record")
@NoArgsConstructor
@AllArgsConstructor
public class PrizeRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID自增
     */
    @TableId
    private Long id;
    /**
     * 会员id
     */
    private Long memberId;
    /**
     * 会员昵称
     */
    private String memberName;
    /**
     * 抽奖花费
     */
    private Integer prizeFee;
    /**
     * 抽奖时间
     */
    private Date createTime;

}
