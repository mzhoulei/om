package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.query.member.PrizeRuleQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zero2oneit.mall.member.mapper.PrizeRuleMapper;
import com.zero2oneit.mall.member.service.PrizeRuleService;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
@Service
public class PrizeRuleServiceImpl extends ServiceImpl<PrizeRuleMapper, PrizeRule> implements PrizeRuleService {

    @Autowired
    private PrizeRuleMapper prizeRuleMapper;

    @Override
    public BoostrapDataGrid ruleList(PrizeRuleQueryObject qo) {
        //查询总记录数
        int total = prizeRuleMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total == 0 ? Collections.EMPTY_LIST : prizeRuleMapper.selectRows(qo));
    }
}