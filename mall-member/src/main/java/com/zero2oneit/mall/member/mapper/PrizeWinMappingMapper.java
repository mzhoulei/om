package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.query.member.PrizeWinMappingQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

/**
 * @author Tg
 * @create 2021-05-25
 * @description
 */
@Mapper
public interface PrizeWinMappingMapper extends BaseMapper<PrizeWinMapping> {

    int selectTotal(PrizeWinMappingQueryObject qo);

    List<HashMap<String,Object>> selectRows(PrizeWinMappingQueryObject qo);
}
